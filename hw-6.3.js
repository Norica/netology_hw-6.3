function Student(name, point) {
  this.name = name;
  this.point = point;
};

Student.prototype.show = function() {
  return console.log('Студент %s набрал %d баллов', this.name, this.point);
};


function StudentList(group, array) {
  if(array == undefined){
    array = [];
  };
  for(var i = 0; i < (array.length); i+=2) {
    this.push(new Student(array[i], array[i+1]));
  };
  this.nameGroup = group;
};

StudentList.prototype = Object.create(Array.prototype);
StudentList.prototype.contsructor = StudentList;
StudentList.prototype.add = function(name, point) {
  this.push(new Student(name, point))
};


var studentsAndPoints = ['Алексей Петров', 0, 
                        'Ирина Овчинникова', 60, 
                        'Глеб Стукалов', 30, 
                        'Антон Павлович', 30, 
                        'Виктория Заровская', 30, 
                        'Алексей Левенец', 70, 
                        'Тимур Вамуш', 30, 
                        'Евгений Прочан', 60, 
                        'Александр Малов', 0];

var hj2 = new StudentList("HJ-2", studentsAndPoints)


hj2.add('Гарри Поттер', 10);
hj2.add('Рон Уизли', 30);
hj2.add('Гермиона Грейнджер', 60);


var html7 = new StudentList('HTML-7');

html7.add('Невилл Долгопупс', 10);
html7.add('Полумна Лавгуд', 20);
html7.add('Джинни Уизли', 40);
html7.add('Дин Томас', 0);
html7.add('Симус Финниган', 20);


StudentList.prototype.show = function() {
  console.log('Группа %s (%d студентов):', this.nameGroup, this.length);
  this.forEach(function(student, i) {
    student.show();
  });
};

html7.show();
hj2.show();


StudentList.prototype.transfer = function(name, group) {
  var index = this.findIndex(function(person) {
    return person.name == name;
  });
  group.push.apply(group, this.splice(index, 1));
};

hj2.transfer('Алексей Петров', html7);


Student.prototype.valueOf = function() {
  return this.point;
}
StudentList.prototype.max = function() {
  var max = Math.max.apply(null, this);
  return this.find(function(student) {
      return student.point == max;
    });
};




